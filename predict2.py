from models import InferSent
from model import *
import torch
import torch.nn as nn
from data_import2 import *

#sentence = "Get a handle on your business financials by learning the ins-and-outs of the three most important financial statements in business accounting. In this session of the Southside First's Echale Gas Series, Carlos Acosta of LiftFund Women’s Business Center will cover basic accounting concepts and terminology. Mr. Acosta will begin with a basic review of accounting essentials and then address the main components of the Financial Statements and how to interpret it them. To conclude, he will discuss how all three of these statements work together to give you a comprehensive picture of your business’s financial position.   The WBC is funded in part through a cooperative agreement with the U.S. Small Business Administration's Office of Women's Business Ownership." #0

#sentence = "Take back your financial power. Start your investing career now; Register Here!   Discover Strategies to Build Wealth the Rich Dad Way Take control of your financial future at this powerful workshop. Based on the teachings of the international #1 bestseller Rich Dad Poor Dad this workshop is designed to give you leading-edge strategies to use real estate investing and the stock market to gain an advantage in today’s economy. No previous experience is required.   Join us and discover strategies to potentially: Move from an “employee” to “entrepreneur” mindset Create long-lasting cash flow from real estate Make money from stocks you don’t even own Spot market opportunities and the right stocks to trade Design an action plan for your financial future And much more!   Following Are the Dates and Locations for the Upcoming Workshops:   Thursday, November 8th, 2018Four Points by Sheraton Meriden 275 Research Parkway Meriden, Connecticut 0645012:30 PM- 2:30 PM6:00 PM- 8:00 PM   World-Class Training from a Rich Dad Expert It’s time to discover the Rich Dad difference. Don’t trust your financial education to just anyone. Rich Dad Education workshops are based on Robert Kiyosaki’s international #1 bestseller Rich Dad Poor Dad and brought to you by Legacy Education™, a world-wide leader in practical financial education for over 24 years. You can be confident knowing your training is based on real-world strategies developed by an international, publicly-traded company dedicated to making a difference in the lives of its students.    Valuable Gifts Just for Attending!  Every attendee receives exclusive access to the Philosophy of Cash Flow Workbook as well as the Legacy Learning Series on MyElitePortal.com. This rotating series of valuable tools and resources is designed to help you get started on the right path.   Pursue Your Dream of Financial Independence Don’t let life or past failures stop you from pursuing your dreams. The rich continue to get rich the same way they always have, by understanding how money works and making their money work for them. Now it’s your turn to learn. No one will ever care more about your money than you. Take back control of your financial future and join us for an eye-opening workshop coming to your area soon. Don’t miss out on this potentially life-changing opportunity. Register now for an upcoming Free Real Estate Workshop coming to your area:   Thursday, November 8th, 2018Four Points by Sheraton Meriden275 Research ParkwayMeriden, Connecticut 0645012:30 PM- 2:30 PM6:00 PM- 8:00 PM   Time is running out. Call 866-302-8053 now or visit RichDadEducation.com to learn more.   “If you work for money, you give the power to your employer. If money works for you, you keep the power and control it.”  -Robert Kiyosaki, Rich Dad Poor Dad      What Our Students Are Saying:    “…It has had a great impact on my life.” -Leo B., Los Angeles, CA" #0
#model = torch.load('models/classification_dataset_1350.pt', map_location='cpu')

# Setting up infersent
#MODEL_PATH = 'encoder/infersent2.pkl'
#params_model = {'bsize': 64, 'word_emb_dim': 300, 'enc_lstm_dim': 2048, 'pool_type': 'max', 'dpout_model': 0.0, 'version': 2}
"""
infersent = InferSent(params_model)
infersent.load_state_dict(torch.load(MODEL_PATH))

print("Loading list of vectors...")
W2V_PATH = 'crawl-300d-2M.vec'
infersent.set_w2v_path(W2V_PATH)

infersent.build_vocab_k_words(K=2000)

"""

cuda = torch.device('cuda')

def predict(infersent, model, sentence, all_labels, n_predictions=3):
    with torch.no_grad():
        embeddings = infersent.encode([sentence], tokenize=True)
        predictions = model(torch.tensor(embeddings, device=cuda))
        softmax = nn.Softmax(dim=1)
        output = softmax(predictions)

    # Get top N categories
    topv, topi = output.max(1)
    predictions = []

    pred_category_index = 0

    """
    for i in range(n_predictions):
        value = topv[0][i].item()
        category_index = topi[0][i].item()
        if i == 0:
            pred_category_index = category_index
        print("category: %s" % category_index)
        print('(%.2f) %s' % (value, all_labels[category_index]))
        predictions.append([value, all_labels[category_index]])
    """
    return all_labels[topi]


def readLines(filename):
    lines = open(filename, encoding='utf-8').read().strip().split('\n')
    return [unicodeToAscii(line) for line in lines]

def computePredictions(infersent, model, all_labels):
    right = 0
    wrong = 0
    print("Testing dataset")
    for filename in findFiles('data-testing-small/*.txt'):
        label = os.path.splitext(os.path.basename(filename))[0]
        lines = readLines(filename)
        for sentence in lines:
            pred_label = predict(infersent, model, sentence, all_labels)
            if label == pred_label:
                #print("Success")
                right += 1
            else:
                #print("Failed Label:"+label+" Pred:"+pred_label)
                wrong += 1
        p = "successrate: " + str(right/(right+wrong) * 100) + "%"
        print(p)

from models import InferSent
from model import *
import torch
import torch.nn as nn
from data_import2 import *
import torch.utils.data
import tqdm

b_size = 10

#sentence = "Get a handle on your business financials by learning the ins-and-outs of the three most important financial statements in business accounting. In this session of the Southside First's Echale Gas Series, Carlos Acosta of LiftFund Women’s Business Center will cover basic accounting concepts and terminology. Mr. Acosta will begin with a basic review of accounting essentials and then address the main components of the Financial Statements and how to interpret it them. To conclude, he will discuss how all three of these statements work together to give you a comprehensive picture of your business’s financial position.   The WBC is funded in part through a cooperative agreement with the U.S. Small Business Administration's Office of Women's Business Ownership."
sentence = "Learn the fundamentals of programming as you get introduced to JavaScript, the most popular language on the job market. Join this workshop and start your journey towards becoming a developer.We'll cover everything you need to know to start learning the language, including different data types, variables, and functions. We'll finish the workshop with a roadmap you can follow to continue learning JavaScript after the meetup. You will receive an email the day of the event with the live webinar link."
model = torch.load('models/classification_dataset_160.pt')

# Setting up infersent
MODEL_PATH = 'encoder/infersent2.pkl'
params_model = {'bsize': 64, 'word_emb_dim': 300, 'enc_lstm_dim': 2048,
                'pool_type': 'max', 'dpout_model': 0.0, 'version': 2}
infersent = InferSent(params_model)
infersent.load_state_dict(torch.load(MODEL_PATH))

print("Loading list of vectors...")
W2V_PATH = 'crawl-300d-2M.vec'
infersent.set_w2v_path(W2V_PATH)

infersent.build_vocab_k_words(K=2000)

dataset = [(sentence, label) for sentence, label in zip(all_sentences[0], labels)]
dataloader = torch.utils.data.DataLoader(dataset, batch_size=b_size, shuffle=False)

all_embeddings = []

for idx, (sentences, _labels) in tqdm.tqdm(enumerate(dataloader)):
    print(idx, len(dataloader))
    with torch.no_grad():
        embeddings = infersent.encode(sentences, tokenize=True)
    all_embeddings.append(torch.tensor(embeddings))

### create single tensor out of list of Tensors
all_embeddings = torch.cat(all_embeddings, dim=0)
torch.save(all_embeddings, "embeddings.pth")

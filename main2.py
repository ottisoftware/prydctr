print("V. 2.5")
import torch
torch.cuda.set_device(0)
from models import InferSent
import nltk

from model import *
import random
from data_import2 import *
from predict2 import computePredictions



# SET UP CUDA
cuda = torch.device('cuda')     # Default CUDA device

nltk.download('punkt')
steps = 1000000

print_every = 10
save_every = 150
batch_size = 5
test_every = 250

device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

MODEL_PATH = 'encoder/infersent2.pkl'
params_model = {'bsize': 64, 'word_emb_dim': 300, 'enc_lstm_dim': 2048,
                'pool_type': 'max', 'dpout_model': 0.0, 'version': 2}

infersent = InferSent(params_model)
infersent.cuda()
infersent.load_state_dict(torch.load(MODEL_PATH))

print("Loading list of vectors...")
W2V_PATH = 'crawl-300d-2M.vec'
infersent.set_w2v_path(W2V_PATH)


def randomChoice(all_s, la):
    global batch_size
    s_batch = []
    l_batch = []
    for i in range(batch_size):
        index = random.randint(0, len(all_s) - (batch_size+1))
        s_batch.extend(all_s[index:index+1])
        l_batch.extend(la[index:index+1])
    return s_batch, torch.tensor(l_batch, device=cuda)

## Returns a Batch of Descriptions + Labels
def randomBatch():
    global all_sentences, cuda
    s_batch, l_batch = randomChoice(all_sentences[0], labels)

    #s_batch = ["Test string 1", "Test string 2"]
    #l_batch = torch.tensor([1, 2])
    return s_batch, l_batch

sentence_batch, labels_batch = randomBatch()


print("Build sentences...")
#infersent.build_vocab(all_sentences[0], tokenize=True)
infersent.build_vocab_k_words(K=10000)
print("create embeddings...")
embeddings = infersent.encode(sentence_batch, tokenize=True)


num_classes = 3
num_inputs = embeddings.shape[1]
print("Ouput size:")
print(num_classes)
model = Classifier(num_inputs, num_classes)


model.to(device)
criterion = nn.CrossEntropyLoss()
optimizer = torch.optim.SGD(model.parameters(), lr=0.1)


print("Start training...")
with torch.cuda.device(0):
    for iter in range(1, steps + 1):
        sentences, label_batch = randomBatch()

        with torch.no_grad():
            embeddings = infersent.encode(sentences, tokenize=True)
        predictions = model(torch.tensor(embeddings, device=cuda))
        loss = criterion(predictions, label_batch)

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        if iter % print_every == 0:
            print('%d %d%% %.4f' % (iter, iter / steps * 100, loss))

        if iter % test_every == 0:
            computePredictions(infersent, model, all_labels)
        if iter % save_every == 0:
            path = 'models/classification_dataset_'+str(iter)+'.pt'
            torch.save(model, path)
            print("saved model")


from __future__ import unicode_literals, print_function, division
from io import open
import glob
import os
import torch

def findFiles(path): return glob.glob(path)

import unicodedata
import string

all_letters = string.ascii_letters + " .,;'"

# Turn a Unicode string to plain ASCII, thanks to http://stackoverflow.com/a/518232/2809427
def unicodeToAscii(s):
    return ''.join(
        c for c in unicodedata.normalize('NFD', s)
        if unicodedata.category(c) != 'Mn'
        and c in all_letters
    )

# Build the category_lines dictionary, a list of names per language
label_lines = {}
all_labels = []

# Read a file and split into lines
def readLines(filename):
    lines = open(filename, encoding='utf-8').read().strip().split('\n')
    return [unicodeToAscii(line) for line in lines]

print("Loading dataset...")
for filename in findFiles('data-training-small/*.txt'):
    label = os.path.splitext(os.path.basename(filename))[0]
    all_labels.append(label)
    lines = readLines(filename)
    label_lines[label] = lines

n_labels = len(all_labels)

print("Imported Labels:")
print(all_labels)

sentence_holder = []
label_holder = []

### Convert it to usefull format ###
for label in all_labels:
    for description in label_lines[label]:
        sentence_holder.extend([description])
        label_holder.extend([int(label)])

if len(sentence_holder) != len(label_holder):
    print("Lables and Data does not match!")
    print(str(len(label_holder))+"/"+str(len(sentence_holder)))
    exit()

#print("Label:")
#print(label_holder[:2])
#print("Sentence:")
#print(sentence_holder[:2])

all_sentences = [sentence_holder]
labels = label_holder

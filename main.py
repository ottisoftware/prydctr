from data_import import *
import torch
from model import *
from training import *
import random
import time
import math
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import atexit


### Plotting ###
n_iters = 100000
print_every = 5000
plot_every = 1000


# Returns the highest category out of an output tensor
def categoryFromOutput(output):
    global all_categories
    top_n, top_i = output.topk(1)
    category_i = top_i[0].item()
    return all_categories[category_i], category_i

def plotTraining():
    plt.figure()
    plt.plot(all_losses)
atexit.register(plotTraining)


n_hidden = 128
#create the network
rnn = RNN(n_letters, n_hidden, n_categories)


#TEST
input = textToTensor('Albert')
hidden = torch.zeros(1, n_hidden)
output, next_hidden = rnn(input[0], hidden) # return hidden
print(categoryFromOutput(output))


###### Trainining Functions #####
learning_rate = 0.005
criterion = nn.NLLLoss() #loss function

def train(category_tensor, line_tensor):
    hidden = rnn.initHidden()
    rnn.zero_grad()

    for i in range(line_tensor.size()[0]):
        output, hidden = rnn(line_tensor[i], hidden)

    loss = criterion(output, category_tensor)
    loss.backward()

    # Add parameters' gradients to their values, multiplied by learning rate
    for p in rnn.parameters():
        p.data.add_(-learning_rate, p.grad.data)

    return output, loss.item()

# Like Text without Backprop
def evaluate(line_tensor):
    hidden = rnn.initHidden()

    for i in range(line_tensor.size()[0]):
        output, hidden = rnn(line_tensor[i], hidden)

    return output


def randomChoice(l):
    return l[random.randint(0, len(l) - 1)]

def randomTrainingExample():
    global all_categories
    category = randomChoice(all_categories)
    line = randomChoice(category_lines[category])
    category_tensor = torch.tensor([all_categories.index(category)], dtype=torch.long)
    line_tensor = textToTensor(line)
    return category, line, category_tensor, line_tensor

### Keep time for plotting ###
def timeSince(since):
    now = time.time()
    s = now - since
    m = math.floor(s / 60)
    s -= m * 60
    return '%dm %ds' % (m, s)

# Keep track of losses for plotting
current_loss = 0
all_losses = []

start = time.time()

for iter in range(1, n_iters + 1):
    category, line, category_tensor, line_tensor = randomTrainingExample()
    output, loss = train(category_tensor, line_tensor)
    current_loss += loss

    # Print iter number, loss, name and guess
    if iter % print_every == 0:
        guess, guess_i = categoryFromOutput(output)
        correct = '✓' if guess == category else '✗ (%s)' % category
        print('%d %d%% (%s) %.4f %s / %s %s' % (iter, iter / n_iters * 100, timeSince(start), loss, line, guess, correct))

    # Add current loss avg to list of losses
    if iter % plot_every == 0:
        all_losses.append(current_loss / plot_every)
        current_loss = 0


torch.save(rnn, 'classification_dataset.pt')
plt.figure()
plt.plot(all_losses)
